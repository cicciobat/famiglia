import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-figlio',
  templateUrl: './figlio.component.html',
  styleUrls: ['./figlio.component.css']
})
export class FiglioComponent {
  @Input() childNumbers!: string[];

  receivedNumbers: string[] = []

  receiveNumbers(event: any): void {
    console.log('app')
    this.receivedNumbers = event;
  }

}
