import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent {
  @Output() sendArray = new EventEmitter<string[]>();
  dadNumbers: string[] = ['I', 'III', 'V', 'VII', 'IX'];



  sendEvent(): void {
    console.log('Sent')
    this.sendArray.emit(this.dadNumbers);
  }
}
